import router from '@/router';

const API_URL = 'http://localhost:3000/';
const LOGIN_URL = `${API_URL}users/login`;
const REGISTER_URL = `${API_URL}users/register`;
const SLASHME_URL = `${API_URL}users/me`;

/* eslint-disable no-param-reassign */

export default {
  user: {
    authenticated: false,
  },
  login(context, creds, redirect) {
    return context.$http.post(LOGIN_URL, creds).then(
      (data) => {
        localStorage.setItem('token', data.body.token);
        this.user.authenticated = true;
        if (redirect) {
          router.go(redirect);
        }
        return this.getAuthenticatedUser(context);
      },
      (err) => {
        context.error = err;
      },
    );
  },
  register(context, creds, redirect) {
    return context.$http.post(REGISTER_URL, creds).then(
      (data) => {
        localStorage.setItem('token', data.body.token);
        this.user.authenticated = true;
        if (redirect) {
          router.go(redirect);
        }
        return this.getAuthenticatedUser(context);
      },
      (err) => {
        context.error = err;
      },
    );
  },
  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    this.user.authenticated = false;
  },
  checkAuth() {
    const jwt = localStorage.getItem('token');
    if (jwt) {
      this.user.authenticated = true;
    } else {
      this.user.authenticated = false;
    }
  },
  getAuthHeader() {
    return {
      Authorization: localStorage.getItem('token'),
    };
  },
  authenticatedUser(context) {
    if (!localStorage.getItem('user')) {
      this.getAuthenticatedUser(context);
    }
    return JSON.parse(localStorage.getItem('user'));
  },
  getAuthenticatedUser(context) {
    return context.$http
      .get(SLASHME_URL, {
        headers: this.getAuthHeader(),
      })
      .then(
        (data) => {
          localStorage.setItem('user', data.bodyText);
        },
        (err) => {
          context.error = err;
        },
      );
  },
};
