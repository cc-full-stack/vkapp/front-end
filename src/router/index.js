import Vue from 'vue';
import Router from 'vue-router';
import Hello from '@/components/Hello';
import Login from '@/components/Login';
import Register from '@/components/Register';
import auth from '@/auth';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello,
      beforeEnter(to, from, next) {
        auth.checkAuth();
        if (auth.user.authenticated) {
          next();
        } else {
          next('/login');
        }
      },
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      beforeEnter(to, from, next) {
        auth.checkAuth();
        if (!auth.user.authenticated) {
          next();
        } else {
          next('/');
        }
      },
    },
    {
      path: '/register',
      name: 'Register',
      component: Register,
      beforeEnter(to, from, next) {
        auth.checkAuth();
        if (!auth.user.authenticated) {
          next();
        } else {
          next('/');
        }
      },
    },
    {
      path: '/logout',
      beforeEnter(to, from, next) {
        auth.logout();
        next('/login');
      },
    },
    {
      path: '*',
      redirect: '/',
    },
  ],
});
